# Arch3D

Página del proyecto "Restauración y conservación de piezas arqueológicas usando aprendizaje profundo y autoencoders convolucionales sobre grafos"

Instituciones
*  Universidad La Salle - Arequipa
*  Pontificia Universidad Católica del Perú


El objetivo de este proyecto es usar un enfoque data-driven para la reparación automática de material arqueológico usando deep-learning